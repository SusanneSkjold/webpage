import { LitElement, html, css } from '../node_modules/lit-element/lit-element';


export class InputBox extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    render() {
        return html`
        <form>
        <div class="input-group mb-3 input-group-lg">
          <div class="input-group-prepend">
            <span class="input-group-text">Your Submission</span>
          </div>
          <textarea id="textcontent" class="form-control" rows="5" id="comment" maxlength="5000"></textarea>
        </div>
      </form>
      <div class="row">
                              <div class="col-sm-8"><div id="placeholder"></div></div>
                              <div class="col-sm-2"><button type="button" onClick="history.go(0);" class="btn btn-dark" window.location.reload()>Cancel</button></div>
                              <div class="col-sm-2"><button type="button" class="btn btn-dark">Submit</button></div> <!-- NB needs to be linked -->
                          </div>
        
      
        `;
    }
}
customElements.define('input-box', InputBox);