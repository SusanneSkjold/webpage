import { LitElement, html, css } from '../node_modules/lit-element/lit-element';


export class PostOptions extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    render() {
        return html`

        <!-- needs a way to know which ones of the options will be displayed, as different users will have it displayed differently -->

        <!-- If user is neither this is displayed: -->

        <div class="btn-group" role="group">                                        <!-- NB needs to be linked -->
            <button type="button" class="btn btn-dark" href="../ReportPost.html">Report</button>
        </div>

        <!-- If user is the creator of the post: -->

        <div class="btn-group" role="group">
            <button type="button" class="btn btn-dark">Edit</button>           <!-- Goes into the make post page, but with everything loaded from the server -->
            <button type="button" class="btn btn-dark">Delete</button>         <!-- Removes the Post from the server -->
            <button type="button" class="btn btn-dark" href="../ReportPost.html">Report</button>         <!-- Adds the post to the list of Reported posts -->
        </div>

        <!-- If user is a moderator: -->

        <div class="btn-group" role="group">
            <button type="button" class="btn btn-dark">Block</button>          <!-- Adds the post to the list of Blockeded posts -->
            <button type="button" class="btn btn-dark" href="../ReportPost.html"> Report</button>
        </div>

        <!-- If user is an Admin: -->

        <div class="btn-group" role="group">
            <button type="button" class="btn btn-dark">Block</button>
            <button type="button" class="btn btn-dark">Delete</button>
            <button type="button" class="btn btn-dark" href="../ReportPost.html">Report</button>
        </div>
      
        `;
    }
}
customElements.define('post-options', PostOptions);