import { LitElement, html, css } from '../node_modules/lit-element/lit-element';
import  '/src/upload_image.js';


export class AddPost extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;
    constructor (){
        super();


    }
    render() {
        return html`

        <head>   
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">  <!-- inkuderer bootstrap /!-->
        </head>
        <body>
        <div class="container">
                <div class="row">
                  <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
                    <div class="card card-signin my-5">
                    <div class="card-body">
                        <h5 class="card-title text-center">Sign up</h5>
                        <form class="form-signin">
                            <div class="form-label-group">
                                <input type="email" id="inputEmail" class="form-control" placeholder="Email address" required autofocus> <!-- Nb:Finn ut variablenavn for user. Ikke emil her, user. koble med usernme i backend-->
                                <label for="inputEmail">Email address</label>
                            </div>

                            <div class="form-label-group">
                                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>         <!-- NB:koble med passowrd i backend-->
                                <label for="inputPassword">Password</label>
                            </div>
                            
                            <div class="form-label-group">
                                <input type="password" id="inputPassword" class="form-control" placeholder="Password" required>         <!-- NB:koble med passowrd i backend-->
                                <label for="inputPassword">Repeat Password</label>
                            </div>

                            <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Sign up</button>
                            <hr class="my-4">
            
                        </form>
                        
                    </div>
                  </div>
                </div>
            </div>


        </body>  
        
        `;
    }
}
customElements.define('add-post', AddPost);