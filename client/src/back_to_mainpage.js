import { LitElement, html, css } from '../node_modules/lit-element/lit-element';


export class BackToMainpage extends LitElement {
    
    static styles = css`
    :host {
        display: block;
    }
    `;
    constructor (){
        super();


    }

    render() {
        return html`

        <head>   
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">  <!-- inkuderer bootstrap /!-->
        </head>

          <a href="/Index" class="btn btn-dark" role="button">Back to mainpage</a> 
                
      
        `;
    }
}
customElements.define('back-to-mainpage', BackToMainpage);