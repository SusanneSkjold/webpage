import { LitElement, html, css } from '../node_modules/lit-element/lit-element';
import  '/src/add_comment.js';
import  '/src/comment_section.js';
import  '/src/vote_buttons.js';
import  '/src/post_options.js';

export class PostContent extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;
    //Denne laster inn innholdet til en post, og kobles her til andre komponenter for a lage hele siden
    //NB inlcude all databases nessecary
    render() {
        return html`

        <div class="container">
             <div class="row">
                    <div class="col-1"></div>
                    <div class="col-6"><post-options></post-options></div>
                    <div class="col-4"><vote-buttons></vote-buttons></div>
             </div>            
             <div class="row">
                    <div class="col-1"></div>
                    <h1 class="col-6">Title NB</h1>
             </div>
             <div class="row">
                     <div class="col-1"></div>
                     <div class="col-8">NB IMAGE find a way to link this to the post</div>
             </div>
             <div class="row">
                    <div class="col-1"></div>
                    <div class="col-8">NB post-content. lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun lorem ipsun</div>

            </div>
            
            <div class="row">
                    <div class="col-1"></div>
                    <div class="col-8">
                        <comment-section></comment-section></div>
                    <div class="col-3">
                        <h2>Your comment:</h2>
                        <add-comment></add-comment>                   
                    </div>    
            </div>
        </div>


                `;
    }
}
customElements.define('post-content', PostContent);