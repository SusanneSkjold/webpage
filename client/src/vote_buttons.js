import { LitElement, html, css } from 'lit-element';


export class VoteButtons extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;
    //Denne har stemmeknappene upvote og downvote, og hvor mange som har upvoted og downvotet valgt element
    render() {
        return html`


        <div class= "row">
            <div class ="col-1">Upvotes NB</div>
            <div class ="col-1"> 
                <img width="30px" src="../resources/upvote.png"></div>          <!--Nb functionality-->
            <div class ="col-1"> 
                <img width="30px" src="../resources/downvote.png"></div>          <!--Nb functionality-->
            <div class ="col-1">Downvotes NB</div>
        </div>

`;
    }
}
customElements.define('vote-buttons', VoteButtons);