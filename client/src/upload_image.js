import { LitElement, html, css } from '../node_modules/lit-element/lit-element';


export class UploadImage extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    render() {
        return html`

        <div class="form-control text-center"> 
            <label class="header">Insert profile picture here:  </label>
                            
            <input id="image" type="file" name="profile_photo" placeholder="Photo" required="" capture>   <!-- NB Needs to add the image to the database-->
                            
            </div> <!-- NB needs to be linked -->
      
        `;
    }
}
customElements.define('upload-image', UploadImage);