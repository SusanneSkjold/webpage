import { LitElement, html, css } from 'lit-element';
import  '/src/vote_buttons.js';
import  '/src/post_options.js';

//NB hva må gjøres her:
    //link bakend
    //Alt må flyttes til elementer, ikke egne html sider
    //Addd functionality that you can click on images to upvote and downvote
    //Include posrt option functionality

//Dette elementet  lister alle poster på et forum.

export class ListeOverPosts extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    render() {
        return html`

        <head>   
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">  <!-- inkuderer bootstrap /!-->
        </head>
        </div>
        <div class="table-bordered">     <!--Box containing post lists/-->
            <div class="container">     <!-- NB for each post, show this content-->
                 <div class="row">
                        <h3 class="col-4">Postname NB</h3>
                        <div class="col-4"></div>
                        <div class="col-4">
                            <div class="text-right"><vote-buttons></vote-buttons></div> <!--nb link til posten-->
                        </div>
                </div>            
                <div class="row">
                        <div class="col-4">Username NB</div>
                </div>
                <div class="row">
                     <div class="col-12">
                         <div class="text-right"><post-options></post-options></div>
                    </div>
                </div>
            </div>
        </div>

        `;
    }
}
customElements.define('liste-over-posts',ListeOverPosts);
