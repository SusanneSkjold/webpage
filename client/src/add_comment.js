import { LitElement, html, css } from '../node_modules/lit-element/lit-element';


export class AddComment extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    //Legger til comment
    render() {
        return html`
            <div class="col-1"></div>
            <div class="col">
                <form>
                <div class="form-group">
                <label for="comment">Comment:</label>
                <textarea class="form-control" rows="5" id="comment"></textarea>
              </div>
                </form>
            </div>
        </div>

        <div class ="row">
            <div class="col-4"></div>
            <div class="col-8">
                <div class="btn-group"  role="group">
                    <button type="button" onClick="history.go(0);" class="btn btn-dark" window.location.reload()>Cancel</button>
                    <button type="button" class="btn btn-dark">Submit</button> <!-- NB needs to be linked -->
                </div>
            </div>
        </div>
        
        `;
    }
}
customElements.define('add-comment', AddComment);