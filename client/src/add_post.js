import { LitElement, html, css } from '../node_modules/lit-element/lit-element';
import  '/src/upload_image.js';


export class AddPost extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;
    constructor (){
        super();


    }
    render() {
        return html`

        <head>   
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">  <!-- inkuderer bootstrap /!-->
        </head>

        <!--NB legg til å uploade bilde og legge til tittel-->
        <div class="input-group mb-3 input-group-lg">
          <textarea id="textcontent" class="form-control" rows="1" id="comment" placeholder="Post headline"  maxlength="300"></textarea>  <!-- NB needs to be linked. Post tittel-->    
        </div>

        <upload-image></upload-image>

        <form>
        <div class="input-group mb-3 input-group-lg">
          <textarea id="textcontent" class="form-control" rows="5" id="comment" maxlength="5000"></textarea>
        </div>
      </form>
      <div class="row">
                 <div class="col-sm-8"><div id="placeholder"></div></div>
                 <div class="col-sm-1"><button type="button" onClick="history.go(0);" class="btn btn-dark" window.location.reload()>Cancel</button></div>
                 <div class="col-sm-1"><button type="button" class="btn btn-dark">Submit</button></div> <!-- NB needs to be linked. Post innhold-->
         </div>
        
      
                
        
        `;
    }
}
customElements.define('add-post', AddPost);