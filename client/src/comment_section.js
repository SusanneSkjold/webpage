import { LitElement, html, css } from 'lit-element';
import  '/src/vote_buttons.js';
export class Commentsection extends LitElement {

    static styles = css`
    :host {
        display: block;
    }
    `;

    render() {
        return html`

         <!-- NB needs to add functionality -->
         <!-- NB includes for each instance of a comment, make structure for now -->

         <h2>Comment Section</h2>           
            <table class="table table-bordered">
                <thead>
                    <tr>
                        <th>User</th>
                        <th>Comment</th>
                        <th>Votesection</th>
                    </tr>
                </thead>
                <tbody>  <!-- NB includes for each instance of a comment, make structure for now -->
                    <tr>
                        <td>
                            <div class="card" style="width:200px">
                                <img class="card-img-bottom" src="https://avatarfiles.alphacoders.com/893/thumb-89303.gif" alt="Card image"> <!-- NB This will need to be linked to users profile image -->
                                    <div  class="col text-center">
                                        <h4 class="card-title"> <p class="text-center"> Username </p> </h4> <!-- NB Username is fetched from the server -->                                
                                    </div>
                                </div>
                            </div>
                        </td>
                        <td>
                        <p> Comment Comment Comment Comment Comment </p> <!-- NB Username is fetched from the server --> 
                        </td>
                        <td><vote-buttons></vote-buttons></td>

                    </tr>
                </tbody>
            </table>
        </div>
        `;
    }
}
customElements.define('comment-section', Commentsection);