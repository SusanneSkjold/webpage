import { LitElement, html, css } from '../node_modules/lit-element/lit-element';


export class BrukerComponent extends LitElement {

    static get properties() {
        return{
            uid: Number,
            user: Object, 
        }
    }

    constructor(){
        super();
        //Get information
        this.uid = 1;
        this.getUser();
    }


    static styles = css`
    :host {
        display: block;
    }
    `;

    //<!-- $(this.user == "Admin") ? html ` if yes ` : html ` if else ` -->


    render() {
        return html`
        
        
        <head>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        </head>
        
        <div class="card" style="width:200px">
            <img class="card-img-top" src="https://avatarfiles.alphacoders.com/893/thumb-89303.gif" alt="Card image"> <!-- NB This will need to be linked to users profile image -->
                <div  class="col text-center">
                    <h4 class="card-title"> <p class="text-center"> Username </p> </h4> <!-- NB Username is fetched from the server -->
                    <a href="/UserSettings.html" class="btn btn-default1" role="button">User Settings</a> 

                    <!-- NB Insert if statement here -->
                    <!-- NB If admin: -->
                    <a href="/ReportedPosts.html" class="btn btn-default1" role="button">Reported Posts</a>
                    <a href="/BlockedPosts.html" class="btn btn-default1" role="button">Blocked Posts</a>

                    <!-- NB if moderator -->
                    <a href="/ModeratorRequests.html" class="btn btn-default1" role="button">Moderator Requests</a>

                
                </div>
            </div>
        </div>
    `};

    getUser(){

        fetch(`${window.MyAppGlobals.serverURL}getUserRole`, { //Gjør kall på url localhost 8081/                  og getUserRole er routen 
            method: 'GET',                              //Henter informasjon                         
            credentials: "include",                     //må alltid være med         
        }).then(res => res.json()                       //Venter til den får svar, gjør om til json
        ).then(res => {
            this.user = res;                            //Sette properties
        }
    )}
}
customElements.define('bruker-component', BrukerComponent);