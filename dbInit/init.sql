/* Create database */
CREATE DATABASE IF NOT EXISTS `prog2053-proj`;

/* Create tables */
CREATE TABLE `users` (
  `uid` BIGINT(8) NOT NULL AUTO_INCREMENT,
  `username` VARCHAR(128) NOT NULL,
  `password` VARCHAR(128) NOT NULL,
  `userType` ENUM('admin', 'moderator', 'user') DEFAULT "user" NOT NULL,
  `picture` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `forums` (
`fid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`user` BIGINT(8) NOT NULL,
`name` VARCHAR(200) NOT NULL,
`numPosts` BIGINT(8) DEFAULT NULL,
PRIMARY KEY (`fid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `posts` (
`pid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`user` BIGINT(8) NOT NULL,
`forum` BIGINT(8) NOT NULL,
`title` VARCHAR(200) NOT NULL,
`content` VARCHAR(20000) NOT NULL,
`picture` varchar(255) DEFAULT NULL,
`upvotes` BIGINT(8) DEFAULT NULL,
`downvotes` BIGINT(8) DEFAULT NULL,
`numComments` BIGINT(8) DEFAULT NULL,
`blocked` TINYINT(1) DEFAULT NULL,
PRIMARY KEY (`pid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`),
FOREIGN KEY(`forum`) REFERENCES forums(`fid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `comments` (
`cid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`post` BIGINT(8) NOT NULL,
`user` BIGINT(8) NOT NULL,
`comment` VARCHAR(20000),
`upvotes` BIGINT(8) DEFAULT NULL,
`downvotes` BIGINT(8) DEFAULT NULL,
`blocked` TINYINT(1) DEFAULT NULL,
PRIMARY KEY (`cid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`),
FOREIGN KEY(`post`) REFERENCES posts(`pid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `moderatorRequests` (
`user` BIGINT(8) NOT NULL,
`request` VARCHAR(20000),
FOREIGN KEY (`user`) REFERENCES users(`uid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `reportedPosts` (
`forum` BIGINT(8) NOT NULL,
`post` BIGINT(8) NOT NULL,
FOREIGN KEY(`forum`) REFERENCES forums(`fid`),
FOREIGN KEY (`post`) REFERENCES posts(`pid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

CREATE TABLE `reportedComments` (
`forum` BIGINT(8) NOT NULL,
`post` BIGINT(8) NOT NULL,
`comment` BIGINT(8) NOT NULL,
FOREIGN KEY(`forum`) REFERENCES forums(`fid`),
FOREIGN KEY (`post`) REFERENCES posts(`pid`),
FOREIGN KEY(`comment`) REFERENCES comments(`cid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;

/* Insert data */
INSERT INTO `users` (`uid`, `username`, `password`, `userType`) VALUES ('1','bartell.martine@example.com','40bcc6f6193986153cae1bb1c36668650a3d5f97','admin'),
('2','zcrona@example.net','1f66d81577cd95514cedc8504d65ec8eff9c336a','moderator'),
('3','wgaylord@example.com','3fcba21eebd2d09681515b4849d2bbeae566451e','user');

INSERT INTO `forums` (`fid`, `user`, `name`) VALUES ('1','3','Dogs'),
('2','2','Cats'),
('3','1','Dogs and cats');

INSERT INTO `posts` (`pid`, `user`, `forum`, `title`, `content`) VALUES ('1','3', '1','cute dog hehe','dog'),
('2','2','2','look at my cat!','cat haha'),
('3','1','3','cute','dog AND cat !!! wow look at that');

INSERT INTO `comments` (`cid`, `post`, `user`, `comment`) VALUES ('1','1','2','yo thats a cool dog'),
('2','2','1','yes go cat'),
('3','3','3','haha look at them');
