
import Router from 'express'
import mysql from 'mysql'
import createSecretKey from 'crypto'

const postRouter = Router()

var db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "password",
    database: "prog2053-proj"
})

db.connect((err) =>{
    if(err) throw err;
})

//Call for å raporterer poster => GET/addReportedPost
/* 
CREATE TABLE `reportedPosts` (
`forum` BIGINT(8) NOT NULL,
`post` BIGINT(8) NOT NULL,
FOREIGN KEY(`forum`) REFERENCES forums(`fid`),
FOREIGN KEY (`post`) REFERENCES posts(`pid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;
*/
// router = anything within path (/*)
//puts post id into reported posts
postRouter.get('/addReportedPost', (req, res) => 
{
    //checks if already in reported posts
    let sql = `SELECT * FROM posts WHERE pid = ${req.body.pid}`;
    db.query(sql, (err, results) => {
        if(err) throw err;
        //if already reported, exits
        if (results.length > 0){
            return res.render('report', {
                message: "Post already reported"
            })
        }
        // else tries to insert posts
        else
        {
            let sql = `INSERT INTO reportedPosts SET post = ${req.body.pid}`;
            db.query(sql, (err, result) =>
            {
                    if(err) throw err;
                    console.log(result);
                    res.send('Post reported');
            })
        }
    })

})
//Call for å avraporterer poster => GET/removeReportedPost
postRouter.get('/removeReportedPost', (req, res) => {

    db.query(`DELETE FROM reportedPosts WHERE post = ${req.body.pid}`, (err, result) =>{ // post is in reported posts
        if(err) throw err;
        console.log(result);
        res.send(`unreported post ${req.body.pid}...`);
    });
});


//Call for å blokere poster => GET/blockPost

postRouter.get('/blockPost', (req, res) => {
    let blocked = 1; // set blocked to 1 (true)
    let sql = `UPDATE posts SET blocked = '${blocked}' WHERE pid = ${req.body.pid}`;
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send(`post ${blockingID} blocked...`);
    })
})

//Call for å avblokere poster => GET/unblockPost


postRouter.get('/blockPost', (req, res) => {
    let blocked = 0; // set blocked to 0 (false)
    let sql = `UPDATE posts SET blocked = '${blocked}' WHERE pid = ${req.body.pid}`;
    let query = db.query(sql, (err, result) => {
        if(err) throw err;
        console.log(result);
        res.send(`post ${req.body.pid} unblocked...`);
    })
})

//Call for å lagre post til database => GET/createPost

postRouter.get('/createPost', (req,res) => {    

    db.query('INSERT INTO posts SET ?', {
        user: req.body.uid,
        name: req.body.name,
        forum: req.body.fid,
        title: req.body.title,
        content: req.body.content,
        picture: req.body.picture
    }, (insertErr, insertRes) => {
        if(insertErr) throw insertErr;
        else {
            res.send(`Forum added`);
        }
    })
    
})


/*
CREATE TABLE `posts` (
`pid` BIGINT(8) NOT NULL AUTO_INCREMENT,
`user` BIGINT(8) NOT NULL,
`forum` BIGINT(8) NOT NULL,
`title` VARCHAR(200) NOT NULL,
`content` VARCHAR(20000) NOT NULL,
`picture` LONGBLOB DEFAULT NULL,
`upvotes` BIGINT(8) DEFAULT NULL,
`downvotes` BIGINT(8) DEFAULT NULL,
`numComments` BIGINT(8) DEFAULT NULL,
`blocked` TINYINT(1) DEFAULT NULL,
PRIMARY KEY (`pid`),
FOREIGN KEY (`user`) REFERENCES users(`uid`),
FOREIGN KEY(`forum`) REFERENCES forums(`fid`)
) ENGINE = InnoDB CHARSET = utf8 COLLATE utf8_bin;
}
*/
export default postRouter