
import express from 'express'
import path from 'path'
import mysql from 'mysql'
import createSecretKey from 'crypto'

const Router = express;
const userRouter = Router()

//userRouter.use(express.static(path.join(__dirname, 'images')))

var db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "password",
    database: "prog2053-proj"
})

db.connect((err) =>{
    if(err) throw err;
})

// Får inn variablene username og password, og lagrer disse som en ny bruker i databasen
userRouter.post('/register', (req,res) =>{

    db.query('SELECT username FROM users WHERE username = ?', [req.body.username], (err, results) =>{
        if(err) throw err;

        if (results.length > 0){
            res.send(`There is already a user with that username`);
        }

        let hashedPass = createSecretKey.createHash(req.body.password)

        db.query('INSERT INTO users SET ?', {
            username: req.body.username,
            password: hashedPass
        }, (insertErr, insertRes) => {
            if(insertErr) throw insertErr;
            else {
                res.send(`User registered`);
            }
        })
    })
})

// Dersom username og password matcher det som er i databasen,
// sendes user id, username, user type og profilbildet tilbake
userRouter.post('/login', (req, res) => {
    const username = req.body.username;

    db.query('SELECT * FROM users WHERE username = ?', [req.body.username], (userErr, userRes) =>{
        if(userErr) throw userErr;

        if (userRes.length == 0 || userRes[1] != createSecretKey.createHash(req.body.password)){
            res.send(`Incorrect username or password`);
        }

        return res.send({
            uid: userRes[0],
            username: userRes[1],
            userType: userRes[3],
            picture: userRes[4],
        })
        
    })

})

// Legger til navnet til et profilbilde i databasen, mappen 'images/'
userRouter.get('/addProfilePicture', (req, res) => {
    var file = req.files.image;
    var img_name = file.name;

    db.query('SELECT * FROM users WHERE uid = ?', [req.body.uid], (userErr, userRes) =>{
        if(userErr) throw userErr;

        if (!req.files){
            return res.status(400).send('No files were uploaded.');
        }
 
		var file = req.files.uploaded_image;
		var img_name=file.name;
 
        // Sjekker at filtypen er riktig
        if(file.mimetype == "image/jpeg" || file.mimetype == "image/png" || file.mimetype == "image/gif" ){
                                
        file.mv('public/images/upload_images/'+file.name, function(err) {
                            
            if (err){
                return res.status(500).send(err);
            }

            var sql = "INSERT INTO users (picture) VALUES ('" + img_name + "')";

                var query = db.query(sql, function(err, result) {
                    res.send(`Added profile picture`);
                });
            });
        } else {
            res.send("This format is not allowed , please upload file with '.png','.gif','.jpg'");
        }
    })

})

// Returnerer navnet på bildet, så det kan hentes med html
// src="http://localhost:8082/images/<%=data[0].picture%>" (tror jeg)
userRouter.get('/getProfilePicture', (req, res) => {

    db.query('SELECT picture FROM users WHERE uid = ?', [req.body.uid], (error, result) =>{
        if(error) throw error;

        res.send({image:result});
    })
})

userRouter.get('/createModeratorRequest', (req,res) =>{

    db.query('INSERT INTO moderatorRequests SET ?', {
        username: req.body.uid,
        request: req.body.message
    }, (error, result) => {
        if(error) throw error;
        else {
            res.send(`Moderator request sent`);
        }
    })
})

// Returnerer en array med enten
//    [ 
//      [user, request],
//      [user, request]
//    ]
// eller
//    [ 
//      [user[0], user[1]],
//      [request[0], request[1]]
//    ]
// jeg skjønner meg ikke på mysql
userRouter.get('/getModeratorRequests', (req,res) =>{
    db.query('SELECT * FROM moderatorRequests', (error, result) =>{
        res.send(JSON.stringify(result));
    })
})

export default userRouter

