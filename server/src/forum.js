
import Router from 'express'
import mysql from 'mysql'
import createSecretKey from 'crypto'

const forumRouter = Router()

var db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "password",
    database: "prog2053-proj"
})

db.connect((err) =>{
    if(err) throw err;
})

// Lager forum, tar inn tre verdier
// fid, uid, name
// forum id, creator user id, navn på forumet
forumRouter.get('/createForum', (req,res) =>{

    db.query('INSERT INTO forums SET ?', {
        fid: req.body.fid,
        user: req.body.uid,
        name: req.body.name
    }, (insertErr, insertRes) => {
        if(insertErr) throw insertErr;
        else {
            res.send(`Forum added`);
        }
    })
})

// Sender alle forumene som json-format
// [
//    [
//      forumid, userid, name, number of posts
//    ]
// ]
forumRouter.get('/getForums', (req,res) =>{

    db.query('SELECT * FROM forums', (error, result) =>{
        res.send(JSON.stringify(result));
    })
})

export default forumRouter