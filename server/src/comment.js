
import Router from 'express'
import mysql from 'mysql'
import createSecretKey from 'crypto'

const commentRouter = Router()

var db = mysql.createConnection({
    host: "db",
    user: "admin",
    password: "password",
    database: "prog2053-proj"
})

db.connect((err) =>{
    if(err) throw err;
})

commentRouter.get('/createComment', (req,res) => {
    if(res.body.comment.size() > 5 && res.body.comment.size() < 250) {      // Checks if comment is between 5 and 250 letters long
        db.query('INSERT INTO comments(post, user, comment) VALUES (?,?,?)', [req.body.pid, req.body.uid, req.body.comment], (err,results,fields) => {
            if(err) throw err;                          // Inserts comment and its data into comments table
            return res.render('createComment', {        // Sends message to frontend
                message: "Comment added"
            })
        })
    } else return res.render('createComment', {         // Sends message to frontend if comment isn't 5 - 250 char long
        message: "Comment too big or smol"
    })
})

commentRouter.get('/getComments', (req,res) => {
    db.query('SELECT * FROM comments WHERE post=? AND blocked=NULL', [req.body.pid], (err,results) => {
        if (err) throw err;                             // Selects comments that belongs to post and isn't blocked
        for (i in results) { // WORKS?                  
            db.query('SELECT username,picture FROM users WHERE uid=?', [results[i].user], (err,results2) => {
                if (err) throw err;                     // Gets username and picture from uid
                results[i].username = results2.username;    // copies username from json object 'results2' over to 'results'
                results[i].picture = results2.picture;      // copies picture from ...
            })
        }
        // Sends to frontend
        return res.render('getComment' , {              // Sends json object to frontend
            username: results.username,                 // How its data is structured:
            picture: results.picture,                   // for (i in results){
            comment: result.comment,                    //      results[i].username     or this??   results.username[i]
            upvotes: results.upvotes,                   //      results[i].picture    i tink dis->  results.picture[i]
            downvotes: results.downvotes,               //      ...                     ¯\_(ツ)_/¯
            cid: results.cid                            // }
        })                                              // every 'i' is a comment
    })
})

commentRouter.get('/addReportedComment', (req,res) => {
    db.query('INSERT INTO reportedComments(forum, post, comment) VALUES (?,?,?)', [req.body.fid, req.body.pid, req.body.cid], (err,results) => {
        if(err) throw err;                              // Inserts which comment is reported into reportedComments table
        return res.render('addReportedComment', {       // Sends message to frontend
            message: "Report submitted"
        })
    })
})                                                      // DUPLICATE REPORTS CAN HAPPEN!!

commentRouter.get('/removeReportedComment', (req,res) => {
    db.query('DELETE FROM reportedComments WHERE forum=? AND post=? AND comment=?', [req.body.fid, req.body.pid, req.body.cid], (err,results) => {
        if(err) throw err;                              // Finds comment with fid,pid,cid and removes it from reportedComments table
        return res.render('removeReportedComment', {    // Sends message to frontend
            message: "Removed report"
        })
    })
})

commentRouter.get('/blockComment', (req,res) => {
    db.query('UPDATE comments SET blocked=? WHERE cid=?', [1, req.body.cid], (err,results) => {
        if(err) throw err;                              // Blocks comment with cid          NB: MUST CALL '/getComments' AGANE TO SHOW CHANGES
        return res.render('blockComment', {             // Sends message to frontend
            message: "Blocked comment"
        })
    })
})

commentRouter.get('/unblockComment', (req,res) => {
    db.query('UPDATE comments SET blocked=? WHERE cid=?', [NULL, req.body.cid], (err,results) => {
        if(err) throw err;                              // Unblocks comment with cid        NB:MUST CALL '/getComments' AGANE TO SHOW CHANGES
        return res.render('blockComment', {             // Sends message to frontend
            message: "Blocked comment"
        })
    })
})
export default commentRouter