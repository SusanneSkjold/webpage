"use strict";

import express from 'express';
import fileUpload from 'express-fileupload';
import path from 'path';
import mysql from 'mysql';
import { default as userRouter } from './src/user.js';
import { default as forumRouter } from './src/forum.js';
import { default as postRouter } from './src/post.js';
import { default as commentRouter } from './src/comment.js';
import { COPYFILE_FICLONE_FORCE } from 'constants';
const app = express();
const PORT = 8081;

app.listen(PORT, () => {
  console.log('Running...');
})

app.use(express.static(path.resolve() + '/server'));
//app.use(express.static(path.join(__dirname, 'images')));
app.use(fileUpload());

var db = mysql.createConnection({
  host: "db",
  user: "admin",
  password: "password",
  database: 'prog2053-proj'
});

db.connect(function (err) {
  if (err) {
    throw err;
  }
  console.log("Connected!");
});

app.get('/', (req, res) => {
  res.send("Hello world");
})

app.use(userRouter);
app.use(forumRouter);
app.use(postRouter);
app.use(commentRouter);


app.get('/getUsers', function (req, res) {
  db.query('SELECT * FROM users', function (err, result) {
    if (err) {
      res.status(400).send('Error in database operation.');
    } else {
      res.writeHead(200, { 'Content-Type': 'application/json' });
      res.end(JSON.stringify(result));
      //console.log("Result: " + res);
    }
  });
});

app.get('/getUserRole', function(req, res){ //Lage session variabel når vi logger in og alltid ha lagret bruker id
  db.query('SELECT * FROM users WHERE userT', function(err, result){
    if (err) {
      res.status(400).send('Error in database operation.');
    } else {

    }
  })
});